#ifndef SET
#define SET

typedef struct node{
	int data;
	struct node * next;
}node;

node * new_node(int data);
void free_node(node * node);
void set_insert(node** head, int data);
void set_delete (node ** head, int data);
void set_print(node * head);
int set_count(node * head);

#endif /* SET */
